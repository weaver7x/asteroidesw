package org.example.asteroidesw;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.example.asteroidesw.R;

public class Preferencias extends PreferenceActivity {

   @Override protected void onCreate(Bundle savedInstanceState) {

      super.onCreate(savedInstanceState);

      addPreferencesFromResource(R.xml.preferencias);

   }
   
   /*SharedPreferences pref = getSharedPreferences ("org.example.asteroides_preferences", MODE_PRIVATE);

   String s = "musica: " + pref.getBoolean("musica", true)
		   +",gr�ficos: " + pref.getString(""graficos", "0")" +
		   		", fragmentos: " + pref.getString("fragmentos", "3");*/
 
}