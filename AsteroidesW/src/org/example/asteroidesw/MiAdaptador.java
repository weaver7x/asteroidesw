package org.example.asteroidesw;

import java.util.Vector;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.asteroidesw.R;

/**
 * En el constructor de la clase se indica la actividad donde se ejecutar� y
 * la lista de datos a visualizar.
 * 
 * El m�todo m�s importante de esta clase es getView()
 * el cual tiene que construir los diferentes Layouts que ser�n a�adidos en la lista.
 * 
 * Comenzamos construyendo un objeto View a partir del c�digo xml definido en elemento_lista.xml.
 * Este trabajo se realiza por medio de la clase LayoutInflater. Luego, se modifica el texto de uno
 * de los TextView seg�n el array que se pas� en el constructor. Finalmente, se obtiene un n�mero
 * al azar (Math.round()) y se asigna uno de los tres gr�ficos de forma aleatoria.
 * @author Weaver
 *
 */
public class MiAdaptador extends BaseAdapter {
    private final Activity actividad;
    private final Vector<String> lista;

    public MiAdaptador(Activity actividad, Vector<String> lista) {
          super();
          this.actividad = actividad;
          this.lista = lista;
    }

    public View getView(int position, View convertView, 
                                     ViewGroup parent) {
          LayoutInflater inflater = actividad.getLayoutInflater();
          View view = inflater.inflate(R.layout.elemento_lista, null,
                                                                                                                             true);
          TextView textView =(TextView)view.findViewById(R.id.titulo);
          textView.setText(lista.elementAt(position));
          ImageView imageView=(ImageView)view.findViewById(R.id.icono);
          switch (Math.round((float)Math.random()*3)){
          case 0:
                 imageView.setImageResource(R.drawable.asteroide1);
                 break;
          case 1:
                 imageView.setImageResource(R.drawable.asteroide2);
                 break;
          default:
                 imageView.setImageResource(R.drawable.asteroide3);
                 break;
          }
          return view;
    }

    public int getCount() {
          return lista.size();
    }

    public Object getItem(int arg0) {
          return lista.elementAt(arg0);
    }

    public long getItemId(int position) {
          return position;
    }
}