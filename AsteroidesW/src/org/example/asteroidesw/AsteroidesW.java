package org.example.asteroidesw;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.example.asteroidesw.R;

public class AsteroidesW extends Activity {
	private Button bAcercaDe;
    /**
     * Hecho por weaver7x
     */
	//private Button salir;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        bAcercaDe =(Button) findViewById(R.id.Button03);
        bAcercaDe.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                 lanzarAcercaDe(null);
           }
        });
        /**
         * Hecho por weaver7x
         */
        /*salir =(Button) findViewById(R.id.Button04);
        salir.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                 lanzarSalir(null);
           }
        });*/
        
        

    }
    
    public void lanzarAcercaDe(View view){
        Intent i = new Intent(this, AcercaDe.class);
              startActivity(i);
    }
    
    /**
     * Hecho por weaver7x
     * @param view
     */
    public void lanzarPreferencias(View view){
        Intent i = new Intent(this, Preferencias.class);
              startActivity(i);
    }
    
    public void  lanzarJuego(View view){
        Intent i = new Intent(this, Juego.class);
        startActivity(i);
    }
    
    /**
     * Hecho por weaver7x
     */
    /*public void lanzarSalir(View view){
    	Intent cerrar = new Intent("Terminar Actividad");
    	sendBroadcast(cerrar);
    }*/
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true; /** true -> el men� ya est� visible */
     }
    
    
    /**
     * Recoger eventos capturados por el menu.
     */
     @Override
     public boolean onOptionsItemSelected(MenuItem item) {
              switch (item.getItemId()) {
              case R.id.acercaDe:
                     lanzarAcercaDe(null);
                     break;              
              case R.id.config:
            	  lanzarPreferencias(null);
            	  break;
              }

              return true; /** true -> consumimos el item, no se propaga*/
     }
     
     public void salir(View view) {
    	 finish();
     }
     
     /**
      * Modulo 3. Guardar puntuaciones
      */
     public static AlmacenPuntuaciones almacen = new AlmacenPuntuacionesArray();
     
     public void lanzarPuntuaciones(View view) {
    	 Intent i = new Intent(this, Puntuaciones.class);
    	 startActivity(i);
     }         
}